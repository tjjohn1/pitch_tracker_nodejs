const cv = require('opencv4nodejs');
const os = require("os");
const cpuCount = /*os.cpus().length*/ 4;
console.log('cpuCount:' + cpuCount);
const Worker = require("tiny-worker");
const Workers = [];

const opencvTest = {

    startTime:0,
    data:[],
    framesInterval:0,
    blueColor:null,
    greenColor:null,
    redColor:null,
    fourcc:null,
    video:null,
    videoName:null,
    cap:null,
    size:null,
    frameRate:null,
    frameCount:null,
    frameResultsFilt:{},
    videoOut:null,
    initX:0,
    initY:0,
    initMaxVal:0,
    framesGroups:[],

    preInit(){
        opencvTest.fourcc = cv.VideoWriter.fourcc('MP4V');
        opencvTest.videoName= 'Ginkel Example';
        opencvTest.videoType= '.mp4';
        opencvTest.video='C:/opencv-test/'+ opencvTest.videoName + opencvTest.videoType;
        opencvTest.cap = new cv.VideoCapture(opencvTest.video);
        opencvTest.size = new cv.Size(opencvTest.cap.get(3), opencvTest.cap.get(4));
        opencvTest.frameRate = opencvTest.cap.get(5);
        opencvTest.frameCount = /*opencvTest.cap.get(7)*/ 300;
        opencvTest.framesInterval = Math.ceil(opencvTest.frameCount/cpuCount);
        opencvTest.blueColor = new cv.Vec3(255,0,0);
        opencvTest.greenColor = new cv.Vec3(0,255,0);
        opencvTest.redColor = new cv.Vec3(0,0,255);
        opencvTest.startTime = new Date();
        opencvTest.data = [];
        opencvTest.videoOut = new cv.VideoWriter('C:/opencv-test/' + opencvTest.videoName + 'BALL_FLIGHT.mp4', opencvTest.fourcc, opencvTest.frameRate, new cv.Size(opencvTest.size.width, opencvTest.size.height), true);
        opencvTest.getFirstMatch().then(result=>{
            opencvTest.initWorkers().then(result=>{
                opencvTest.init();
            });
        });
    },

    getFirstMatch(){
        return new Promise((resolve, reject) => {
            let worker = new Worker('C:/Users/tjohnson/WebstormProjects/opencv-convert/public/javascripts/opencv-worker.js', [], {esm: true});
            let realCount = opencvTest.cap.get(7);
            let frameStart = realCount / 2;
            worker.postMessage({
                cmd: 'matchFrameInit',
                params: {
                    videoName: opencvTest.videoName,
                    videoType: opencvTest.videoType,
                    frameStart: frameStart,
                    frameEnd: realCount - 1
                }
            });
            worker.onmessage = function (ev) {
                if (ev.data.initVals) {
                    opencvTest.initX = ev.data.initVals.x;
                    opencvTest.initY = ev.data.initVals.y;
                    opencvTest.initMaxVal = ev.data.initVals.max;
                    resolve(true);
                }
            };
        })
    },

    initWorkers(){
        return new Promise((resolve, reject) => {
            for(let i = 1; i <= cpuCount; i++){
                let worker = new Worker('C:/Users/tjohnson/WebstormProjects/opencv-convert/public/javascripts/opencv-worker.js', [], {esm: true});
                let frameStart = (i - 1)*opencvTest.framesInterval + 600;
                let frameEnd = Math.min(frameStart + opencvTest.framesInterval - 1, opencvTest.frameCount - 1 + 600);
                worker.postMessage({cmd:'init',params:{videoName:opencvTest.videoName, videoType:opencvTest.videoType, frameStart:frameStart, frameEnd:frameEnd, initX:opencvTest.initX, initY:opencvTest.initY, initMaxVal:opencvTest.initMaxVal}});
                Workers.push(worker);
                if(i === cpuCount){
                    console.log('initialized workers');
                    resolve(true);
                }
            }
        })
    },

    timeElapsed() {
        let endTime = new Date();
        let timeDiff = endTime - opencvTest.startTime;
        timeDiff /= 1000;
        return Math.round(timeDiff);
    },

    init(){
        opencvTest.startTime = new Date();
        function checkDone(state) {
            return state;
        }
        let promises = [];
        for(let i = 1; i <= cpuCount; i++){
            promises.push(false);
            Workers[i - 1].postMessage({cmd:'matchFrames'});
            Workers[i - 1].onmessage = function (ev) {
                if(ev.data.result){
                    //console.log(ev.data.result);
                    Workers[i - 1].terminate();
                    //console.log('worker ' + i + ' done in ' + opencvTest.timeElapsed() + 's');
                    promises[i - 1] = true;
                    Object.assign(opencvTest.frameResultsFilt, opencvTest.filterFrames(ev.data.result.arr, ev.data.result.highestMax));
                    if(promises.every(checkDone)){
                        let finalArr = [];
                        let keys = Object.keys(opencvTest.framesGroups);
                        let length = keys.length, lengthMin1 = length - 1;
                        for(let j = 0; j < length; j++){
                            if(opencvTest.framesGroups[keys[j]].length > finalArr.length){
                                finalArr = opencvTest.framesGroups[keys[j]];
                            }
                            if(j === lengthMin1){
                                console.log('done processing frames in ' + opencvTest.timeElapsed() + 's');
                                console.log('accepted frames: ' + Object.keys(opencvTest.frameResultsFilt).length);
                                //console.log(opencvTest.frameResultsFilt);
                                console.log('longest accepted frame sequence:');
                                console.log(finalArr);
                                opencvTest.outputVideo();
                            }
                        }
                    }
                }
            };
        }
    },

    filterFrames(arr, highestMax){
        let counter = 0;
        let keys = Object.keys(opencvTest.framesGroups);
        if(keys.length > 0){
            counter = keys[keys.length - 1];
        }
        let lastFailed = false;
        //console.log(arr);
        return arr.reduce(function(buckets,frameResult){
            if((highestMax - frameResult.minMaxObj.maxVal) < 0.3 && frameResult.detectedCircle != null){
                if(!opencvTest.framesGroups.hasOwnProperty(counter)){
                    opencvTest.framesGroups[counter] = [frameResult.frameNum];
                } else {
                    opencvTest.framesGroups[counter].push(frameResult.frameNum);
                }
                buckets[frameResult.frameNum] = frameResult;
                lastFailed = false;
            } else {
                if(!lastFailed){
                    ++counter;
                }
                lastFailed = true;
            }
            return buckets;
        },{})
    },

    outputVideo(){
        opencvTest.cap.set(1,600/*0*/);
        let counter = 600/*0*/;
        let realFrameCount = opencvTest.frameCount + 600;
        while(counter <= realFrameCount){
            ++counter;
            let frameResult = opencvTest.frameResultsFilt[counter - 1];
            if(frameResult != null){
                //console.log(frameResult);
                //console.log('frame ' + counter + ' has result');
                opencvTest.frameHandling(opencvTest.cap.read(), frameResult.detectedCircle, [frameResult.minMaxObj.maxLoc.x,frameResult.minMaxObj.maxLoc.y], frameResult.bottomRight, counter, realFrameCount);
            } else {
                //console.log('frame ' + counter + ' doesn\'t have result');
                opencvTest.videoOut.write(opencvTest.cap.read());
                if(counter === realFrameCount){
                    opencvTest.videoOut.release();
                    console.log('video writing done in ' + opencvTest.timeElapsed() + 's');
                }
            }
        }
    },

    frameHandling(frame, detectedCircle, topLeft, bottomRight, curFramePos, realFrameCount){
        //console.log(detectedCircle);
        let rect = new cv.Rect(topLeft[0], topLeft[1], Math.abs(bottomRight[0] - topLeft[0]), Math.abs(bottomRight[1] - topLeft[1]));
        frame.drawRectangle(rect, opencvTest.blueColor, 2);
        let circPoint = new cv.Point2(detectedCircle.x + topLeft[0], detectedCircle.y + topLeft[1]);
        frame.drawCircle(circPoint, detectedCircle.z, opencvTest.greenColor, 2);
        frame.drawCircle(circPoint, 1, opencvTest.redColor, 3);
        opencvTest.videoOut.write(frame);
        if(curFramePos === realFrameCount){
            opencvTest.videoOut.release();
            console.log('video writing done in ' + opencvTest.timeElapsed() + 's');
        }
    },

    addData(curFramePos, curFrameMsec, max_val, top_left, detected_circlesLength, highest_max_val, a, b, r){
        try{
            opencvTest.data.push([curFramePos, max_val, top_left[0], top_left[1], ((max_val > .65) && (detected_circlesLength > 0)), (highest_max_val-max_val), highest_max_val, a, b, r, curFrameMsec])
        } catch(exception){
            opencvTest.data.push([curFramePos, max_val, top_left[0], top_left[1], ((max_val > .65) && (detected_circlesLength > 0)), (highest_max_val-max_val), highest_max_val, 0, 0, 0, curFrameMsec])
        }
    },
};

module.exports = opencvTest;