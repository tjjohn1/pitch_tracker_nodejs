import * as cv from 'opencv4nodejs';

onmessage = function (ev) {
    let data = ev.data;
    switch (data.cmd) {
        case 'init':
            console.log('WORKER STARTED INIT');
            init(data.params.videoName,data.params.videoType, data.params.frameStart, data.params.frameEnd, data.params.initX, data.params.initY, data.params.initMaxVal).then(result=>{
                self.postMessage({initialized:true});
            });
            break;
        case 'matchFrameInit':
            console.log('WORKER STARTED MATCH FRAME INIT');
            init(data.params.videoName,data.params.videoType, data.params.frameStart, data.params.frameEnd).then(result=>{
                getGoodStart().then(result=>{
                    console.log('WORKER DONE MATCH FRAME INIT');
                    self.postMessage({initVals:result});
                });
            });
            break;
        case 'matchFrames':
            console.log('WORKER STARTED MATCH FRAME');
            matchFrames().then(result=>{
                console.log('WORKER DONE MATCH FRAME');
                console.log('frame_start: ' + frame_start + ', frame_end: ' + frame_end);
                self.postMessage({result:result});
            });
            break;
    }
};

let threeByThreeSize = new cv.Size(3,3);
let fiveByFiveSize = new cv.Size(5,5);
let sigmaX = 3;
let backSub = new cv.BackgroundSubtractorMOG2();
let houghGradient = cv.HOUGH_GRADIENT;
let coeffNormed = cv.TM_CCOEFF_NORMED;
let morphOpen = cv.MORPH_OPEN;
let bb_template = cv.imread('C:/opencv-test/Baseball Template.png',0);
let bb_template_gray = cv.imread('C:/opencv-test/Baseball Template_Gray.png',0);
let detected_circles_template = bb_template_gray.houghCircles(houghGradient, 1, 20, 50, 20, 0, 0);
let radius_bb_template = parseInt(detected_circles_template[0].z);
let w = bb_template.sizes[1];
let h = bb_template.sizes[0];
let last_max_val = 0;
let max_val = 0;
let highest_max_val = 0;
let stored_top_left = [0, 0];
let cap;
let frame_start;
let frame_end;

async function init(video_name, video_type, frameStart, frameEnd, initX, initY, initMaxVal){
    return new Promise((resolve, reject) => {
        //frame_count = frameCount;
        if(initMaxVal){
            last_max_val = initMaxVal;
            highest_max_val = initMaxVal;
        }
        if(initX && initY){
            stored_top_left = [initX, initY];
        }
        frame_start = frameStart;
        frame_end = frameEnd;
        //videoOut = new cv.VideoWriter('C:/opencv-test/' + video_name + 'BALL_FLIGHT.mp4', fourcc, frameRate, new cv.Size(size.width, size.height), true);
        let video='C:/opencv-test/'+ video_name + video_type;
        cap = new cv.VideoCapture(video);
        cap.set(1,frame_start);
        console.log('frame_start: ' + frame_start + ', frame_end: ' + frame_end);
        resolve(true);
    })
}

function autoCanny(image){
    let mean = image.meanStdDev().mean.getDataAsArray()[0][0];
    let lower = parseInt(Math.max(0, (1.0 - 0.33) * mean));
    let upper = parseInt(Math.min(255, (1.0 + 0.33) * mean));
    return image.canny(lower, upper);
}

async function matchFrames(){
    let returnArr = [];
    return new Promise((resolve, reject) => {
        for(let i = frame_start; i <= frame_end; i++) {
            let frame = cap.read();
            let gray_frame = frame.cvtColor(cv.COLOR_BGR2GRAY);
            let fgMask = backSub.apply(gray_frame);
            let kernelAdj = cv.getStructuringElement(morphOpen, threeByThreeSize);
            let gray_frame_adjMorph = gray_frame.morphologyEx(kernelAdj, morphOpen);
            let gray_frame_adjGauss = gray_frame_adjMorph.gaussianBlur(fiveByFiveSize, sigmaX);
            let gray_frame_adj = autoCanny(gray_frame_adjGauss);
            if ((highest_max_val !== 0) && (highest_max_val - max_val < 0.25) && (highest_max_val > .6) && (stored_top_left !== [0, 0])) {
                try {
                    let startH = stored_top_left[1] - 20, endH = Math.min(stored_top_left[1] + h + 20, 1080),
                        startW = stored_top_left[0] - 20, endW = Math.min(stored_top_left[0] + w + 20, 1920);
                    let rect = new cv.Rect(startW, startH, Math.abs(endW - startW), Math.abs(endH - startH));
                    let section = fgMask.getRegion(rect);
                    let res = bb_template.matchTemplate(section, coeffNormed);
                    let minMaxObj = cv.minMaxLoc(res);
                    max_val = minMaxObj.maxVal;
                    let max_loc = minMaxObj.maxLoc;
                    let top_left = [startW + max_loc.x, startH + max_loc.y];
                    let bottom_right = [top_left[0] + w, top_left[1] + h];
                    let detected_circles = gray_frame_adj.houghCircles(houghGradient, 1, 50, 25, 15, radius_bb_template - 3, radius_bb_template + 8);
                    if (max_val > highest_max_val) {
                        highest_max_val = max_val;
                    }
                    if (detected_circles.length > 0) {
                        stored_top_left = top_left;
                    }
                    returnArr.push({
                        frameNum:i,
                        topLeft:top_left,
                        bottomRight:bottom_right,
                        minMaxObj:minMaxObj,
                        detectedCircle:detected_circles[0]
                    });
                    if(i === frame_end){
                        resolve({arr:returnArr, highestMax:highest_max_val});
                    }
                } catch (exception) {
                    let startH = stored_top_left[1], endH = stored_top_left[1] + h,
                        startW = stored_top_left[0], endW = stored_top_left[0] + w;
                    let rect = new cv.Rect(startW, startH, Math.abs(endW - startW), Math.abs(endH - startH));
                    let section = fgMask.getRegion(rect);
                    let res = bb_template.matchTemplate(section, coeffNormed);
                    let minMaxObj = cv.minMaxLoc(res);
                    max_val = minMaxObj.maxVal;
                    let max_loc = minMaxObj.maxLoc;
                    let top_left = [startW + max_loc.x, startH + max_loc.y];
                    let bottom_right = [top_left[0] + w, top_left[1] + h];
                    let detected_circles = gray_frame_adj.houghCircles(houghGradient, 1, 50, 25, 15, radius_bb_template - 3, radius_bb_template + 8);
                    if (max_val > highest_max_val) {
                        highest_max_val = max_val;
                    }
                    if (detected_circles.length > 0) {
                        stored_top_left = top_left;
                    }
                    returnArr.push({
                        frameNum:i,
                        topLeft:top_left,
                        bottomRight:bottom_right,
                        minMaxObj:minMaxObj,
                        detectedCircle:detected_circles[0]
                    });
                    if(i === frame_end){
                        resolve({arr:returnArr, highestMax:highest_max_val});
                    }
                }
            } else {
                let res = bb_template.matchTemplate(fgMask, coeffNormed);
                let minMaxObj = cv.minMaxLoc(res);
                let max_val = minMaxObj.maxVal;
                let max_loc = minMaxObj.maxLoc;
                let top_left = [max_loc.x, max_loc.y];
                let bottom_right = [top_left[0] + w, top_left[1] + h];
                let startH = top_left[1], endH = top_left[1] + h,
                    startW = top_left[0], endW = top_left[0] + w;
                let rect = new cv.Rect(startW, startH, Math.abs(endW - startW), Math.abs(endH - startH));
                let section = gray_frame_adj.getRegion(rect);
                let detected_circles = section.houghCircles(houghGradient, 1, 50, 25, 15, radius_bb_template - 3, radius_bb_template + 8);
                if (max_val > highest_max_val) {
                    highest_max_val = max_val;
                }
                if (detected_circles.length > 0) {
                    stored_top_left = top_left;
                }
                returnArr.push({
                    frameNum:i,
                    topLeft:top_left,
                    bottomRight:bottom_right,
                    minMaxObj:minMaxObj,
                    detectedCircle:detected_circles[0]
                });
                if(i === frame_end){
                    resolve({arr:returnArr, highestMax:highest_max_val});
                }
            }
        }
    });
}

async function getGoodStart(){
    let maxLoc = new cv.Point2(0,0);
    let maxVal = 0;
    return new Promise((resolve, reject) => {
        for(let i = frame_start; i <= frame_end; i++) {
            //console.log('highest_max_val: ' + highest_max_val + ', max_val: ' + max_val);
            let frame = cap.read();
            if(frame_start === 0){
                //console.log('good start frame: ' + cap.get(cv.CAP_PROP_POS_FRAMES) + ', i: ' + i);
            }
            let gray_frame = frame.cvtColor(cv.COLOR_BGR2GRAY);
            let fgMask = backSub.apply(gray_frame);
            let kernelAdj = cv.getStructuringElement(morphOpen, threeByThreeSize);
            let gray_frame_adjMorph = gray_frame.morphologyEx(kernelAdj, morphOpen);
            let gray_frame_adjGauss = gray_frame_adjMorph.gaussianBlur(fiveByFiveSize, sigmaX);
            let gray_frame_adj = autoCanny(gray_frame_adjGauss);
            if ((highest_max_val !== 0) && (highest_max_val - max_val < 0.25) && (highest_max_val > .6) && (stored_top_left !== [0, 0])) {
                let res = bb_template.matchTemplate(fgMask, coeffNormed);
                let minMaxObj = cv.minMaxLoc(res);
                maxLoc = minMaxObj.maxLoc;
                maxVal = minMaxObj.maxVal;
                resolve({x:maxLoc.x, y:maxLoc.y, max:maxVal});
                i = frame_end;
            } else {
                let res = bb_template.matchTemplate(fgMask, coeffNormed);
                let minMaxObj = cv.minMaxLoc(res);
                //console.log(minMaxObj);
                max_val = minMaxObj.maxVal;
                let max_loc = minMaxObj.maxLoc;
                let top_left = [max_loc.x, max_loc.y];
                let startH = top_left[1], endH = top_left[1] + h,
                    startW = top_left[0], endW = top_left[0] + w;
                let rect = new cv.Rect(startW, startH, Math.abs(endW - startW), Math.abs(endH - startH));
                let section = gray_frame_adj.getRegion(rect);
                let detected_circles = section.houghCircles(houghGradient, 1, 50, 25, 15, radius_bb_template - 3, radius_bb_template + 8);
                if (max_val > highest_max_val) {
                    highest_max_val = max_val;
                }
                if (detected_circles.length > 0) {
                    stored_top_left = top_left;
                    maxLoc = minMaxObj.maxLoc;
                    maxVal = minMaxObj.maxVal;
                }
                if(i === frame_end){
                    resolve({x:maxLoc.x, y:maxLoc.y, max:maxVal});
                }
            }
        }
    });
}